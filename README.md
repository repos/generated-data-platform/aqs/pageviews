# Pageviews API

Pageviews is a public API developed and maintained by the Wikimedia Foundation that serves analytical
data about article pageviews of Wikipedia and its sister projects. With it, you can get pageview
trends on specific articles or projects; filter by agent type or access method, and choose different
time ranges and granularities; you can also get the most viewed articles of a certain project and
timespan, and even check out the countries that visit a project the most.

# Project structure

In order to increase maintainibility and decrease tight coupling, the project is structured within the following layers
entities -> This contains the main resources for which the API is developed.They are the least likely to change when something external changes. For example, you would not expect your per-article entity to change just because you want to show pageviews details according to browsers.
Mainly contains struct within the go file.
logic -> This contains the main functional and processing logic for each entity in use. depends on the entities. change in the entities will require a change in the use case layer, but changes to other layers won’t.
test -> This directory contains test around each handler function.
Complete documentation can be found on [Wikitech][wikitech].

## Build, Test, & Run

You will need:

- Go
- Make

Build:

```sh-session
$ make
[...]
$
```

Test ([requires Cassandra](#dockerized_test_environment)):

```sh-session
$ make CASSANDRA_HOST=xxx.xx.xxx.xx test
[...]
$
```

Run ([also requires Cassandra](#dockerized_test_environment)):

```sh-session
$ ./pagviews
[...]
```

## Prometheus metrics

A resource serving Prometheus request count and latency metrics can be found at `/admin/metrics`.

## Dockerized Cassandra test environment

A Dockerized, single-node Cassandra test environment is included in this repo, including tooling to
create the Pageviews schema and load a small sample set. To start the environment (requires
[Docker Compose][docker_compose]):

```sh-session
$ make -C docker startup
[ ... ]
cassandra_1  | INFO  [main] 2021-08-11 00:07:25,496 Gossiper.java:1832 - Waiting for gossip to settle...
cassandra_1  | INFO  [main] 2021-08-11 00:07:33,498 Gossiper.java:1863 - No gossip backlog; proceeding
cassandra_1  | INFO  [main] 2021-08-11 00:07:33,751 NativeTransportService.java:68 - Netty using native Epoll event loop
cassandra_1  | INFO  [main] 2021-08-11 00:07:33,799 Server.java:158 - Using Netty Version: [netty-buffer=netty-buffer-4.0.44.Final.452812a, netty-codec=netty-codec-4.0.44.Final.452812a, netty-codec-haproxy=netty-codec-haproxy-4.0.44.Final.452812a, netty-codec-http=netty-codec-http-4.0.44.Final.452812a, netty-codec-socks=netty-codec-socks-4.0.44.Final.452812a, netty-common=netty-common-4.0.44.Final.452812a, netty-handler=netty-handler-4.0.44.Final.452812a, netty-tcnative=netty-tcnative-1.1.33.Fork26.142ecbb, netty-transport=netty-transport-4.0.44.Final.452812a, netty-transport-native-epoll=netty-transport-native-epoll-4.0.44.Final.452812a, netty-transport-rxtx=netty-transport-rxtx-4.0.44.Final.452812a, netty-transport-sctp=netty-transport-sctp-4.0.44.Final.452812a, netty-transport-udt=netty-transport-udt-4.0.44.Final.452812a]
cassandra_1  | INFO  [main] 2021-08-11 00:07:33,799 Server.java:159 - Starting listening for CQL clients on /0.0.0.0:9042 (unencrypted)...
cassandra_1  | INFO  [main] 2021-08-11 00:07:33,828 CassandraDaemon.java:564 - Not starting RPC server as requested. Use JMX (StorageService->startRPCServer()) or nodetool (enablethrift) to start it
cassandra_1  | INFO  [main] 2021-08-11 00:07:33,828 CassandraDaemon.java:650 - Startup complete
```

Wait until you see the 'Startup complete' log message, then in another terminal, bootstrap the
database schema and sample data:

```sh-session
$ make -C docker bootstrap
[ ... ]
```

## API documentation

To generate and view the API documentation, read the [docs on Wikitech][wikipage].

[wikitech]: https://wikitech.wikimedia.org/wiki/Analytics/AQS/Pageviews
[docker_compose]: https://docs.docker.com/compose/
[wikipage]: https://wikitech.wikimedia.org/wiki/AQS_2.0#API_documentation
