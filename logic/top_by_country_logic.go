package logic

import (
	"context"
	"encoding/json"
	"math"
	"pageviews/entities"
	"strconv"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/valyala/fasthttp"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

type TopByCountryLogic struct {
}

func (s *TopByCountryLogic) ProcessTopByCountryLogic(context context.Context, ctx *fasthttp.RequestCtx, project, access, year, month, requestURI string, requestLogger *logger.Logger, session *gocql.Session) *entities.TopByCountryResponse {
	var err error
	var countriesJSON string
	var countriesData = []entities.CountryData{}
	var response = entities.TopByCountryResponse{Items: make([]entities.TopByCountry, 0)}

	query := `SELECT "countriesJSON" FROM "local_group_default_T_top_bycountry".data WHERE "_domain" = 'analytics.wikimedia.org' AND project = ? AND access = ? AND year = ? AND month = ?`
	if err := session.Query(query, project, access, year, month).WithContext(context).Consistency(gocql.One).Scan(&countriesJSON); err != nil {
		requestLogger.Log(logger.ERROR, "Query failed; %s", err)
		aqsassist.InternalServerErrorWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: err.Error(),
			URL:    &requestURI,
		})
		return nil
	}

	if err = json.Unmarshal([]byte(countriesJSON), &countriesData); err != nil {
		requestLogger.Log(logger.ERROR, "Unable to unmarshal returned data: %s", err)
		aqsassist.InternalServerErrorWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: err.Error(),
			URL:    &requestURI,
		})
		return nil
	}

	var countries = []entities.Country{}

	for _, s := range countriesData {
		countries = append(countries, entities.Country{
			Country:   s.Country,
			Views:     getIntervalForCeilValue(s.Views),
			Rank:      s.Rank,
			ViewsCiel: s.Views,
		})
	}

	response.Items = append(response.Items, entities.TopByCountry{
		Project:   project,
		Access:    access,
		Year:      year,
		Month:     month,
		Countries: countries,
	})

	return &response
}

func getIntervalForCeilValue(value int) string {

	if value < 100 {
		return "100-999"
	}

	//If value is exactly a power of 10, round down
	if math.Mod(math.Log10(float64(value)), 1) == 0 {
		return getIntervalForCeilValue(value - 1)
	}

	var valueString = strconv.FormatInt(int64(value), 10)
	var valueLength = len(valueString)
	var start = math.Pow10(valueLength - 1)
	var end = math.Pow10(valueLength) - 1
	return (strconv.FormatFloat(start, 'f', 0, 64) + "-" + strconv.FormatFloat(end, 'f', 0, 64))
}
