package logic

import (
	"context"
	"fmt"
	"pageviews/entities"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/valyala/fasthttp"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

type PerArticleLogic struct {
}

func (s *PerArticleLogic) ProcessPerArticleLogic(context context.Context, ctx *fasthttp.RequestCtx, access, agent, project, article, granularity, start, end, requestURI, aggregate string, requestLogger *logger.Logger, session *gocql.Session) *entities.PerArticleResponse {
	var err error
	var response = entities.PerArticleResponse{Items: make([]entities.PerArticle, 0)}
	query := fmt.Sprintf(`SELECT %s, timestamp FROM "local_group_default_T_pageviews_per_article_flat".data WHERE "_domain" = 'analytics.wikimedia.org' AND project = ? AND article = ? AND granularity = ? AND timestamp >= ? AND timestamp <= ?`, aggregate)
	scanner := session.Query(query, project, article, granularity, start, end).WithContext(context).Iter().Scanner()
	var views int
	var timestamp string

	for scanner.Next() {
		if err = scanner.Scan(&views, &timestamp); err != nil {
			requestLogger.Log(logger.ERROR, "Query failed: %s", err)
			aqsassist.InternalServerErrorWrapper(aqsassist.ValidationWrapper{
				Ctx:    ctx,
				Detail: err.Error(),
				URL:    &requestURI,
			})
			return nil

		}
		response.Items = append(response.Items, entities.PerArticle{
			Project:     project,
			Article:     article,
			Granularity: granularity,
			Timestamp:   timestamp,
			Access:      access,
			Agent:       agent,
			Views:       views,
		})
	}

	if err := scanner.Err(); err != nil {
		requestLogger.Log(logger.ERROR, "Error querying database: %s", err)
		aqsassist.InternalServerErrorWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: err.Error(),
			URL:    &requestURI,
		})
		return nil
	}

	return &response
}
