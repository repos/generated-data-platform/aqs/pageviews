package logic

import (
	"context"
	"pageviews/entities"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/valyala/fasthttp"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

type PerProjectLogic struct {
}

func (s *PerProjectLogic) ProcessPerProjectLogic(context context.Context, ctx *fasthttp.RequestCtx, project, access, agent, granularity, start, end, requestURI string, requestLogger *logger.Logger, session *gocql.Session) *entities.PerProjectResponse {
	var err error
	var response = entities.PerProjectResponse{Items: make([]entities.PerProject, 0)}

	query := `SELECT v, timestamp FROM "local_group_default_T_pageviews_per_project_v2".data WHERE "_domain" = 'analytics.wikimedia.org' AND project = ? AND access = ? AND agent = ? AND granularity = ? AND timestamp >= ? AND timestamp <= ?`
	scanner := session.Query(query, project, access, agent, granularity, start, end).WithContext(context).Iter().Scanner()
	var views int
	var timestamp string

	for scanner.Next() {
		if err = scanner.Scan(&views, &timestamp); err != nil {
			requestLogger.Log(logger.ERROR, "Query failed: %s", err)
			aqsassist.InternalServerErrorWrapper(aqsassist.ValidationWrapper{
				Ctx:    ctx,
				Detail: err.Error(),
				URL:    &requestURI,
			})
			return nil
		}
		response.Items = append(response.Items, entities.PerProject{
			Project:     project,
			Access:      access,
			Agent:       agent,
			Granularity: granularity,
			Timestamp:   timestamp,
			Views:       views,
		})
	}

	if err := scanner.Err(); err != nil {
		requestLogger.Log(logger.ERROR, "Error querying database: %s", err)
		aqsassist.InternalServerErrorWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: err.Error(),
			URL:    &requestURI,
		})
		return nil
	}
	return &response
}
