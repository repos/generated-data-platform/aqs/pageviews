module pageviews

go 1.15

require (
	gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang v0.0.0-20220322011350-df509f780b5c
	github.com/carousell/fasthttp-prometheus-middleware v1.0.6
	github.com/fasthttp/router v1.4.11
	github.com/go-openapi/spec v0.20.7 // indirect
	github.com/go-openapi/swag v0.22.3 // indirect
	github.com/gocql/gocql v1.2.1
	github.com/golang/snappy v0.0.4 // indirect
	github.com/klauspost/compress v1.15.15 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.2 // indirect
	github.com/prometheus/client_golang v1.13.0 // indirect
	github.com/roger-russel/fasthttp-router-middleware v1.0.0
	github.com/stretchr/testify v1.8.1
	github.com/swaggo/swag v1.8.6
	github.com/valyala/fasthttp v1.44.0
	gitlab.wikimedia.org/frankie/aqsassist v0.0.0-20230123112424-06f5409b2b38
	golang.org/x/sys v0.0.0-20221006211917-84dc82d7e875 // indirect
	golang.org/x/tools v0.1.12 // indirect
	gopkg.in/yaml.v2 v2.4.0
	schneider.vip/problem v1.8.0
)
