package entities

// TopByCountry represents the top-by-country resultset.
type TopByCountry struct {
	Project   string    `json:"project" example:"en.wikipedia.org"` // Wikimedia project domain
	Access    string    `json:"access" example:"all-access"`        // Access method
	Year      string    `json:"year" example:"2023"`                // Year to be included
	Month     string    `json:"month" example:"01"`                 // Month to be included
	Countries []Country `json:"countries"`
}

// CountryData represents an individual country's result in the set.
type CountryData struct {
	Country string `json:"country" example:"Ghana"` // Country involved
	Views   int    `json:"views" example:"11609"`   // Number of pageviews
	Rank    int    `json:"rank"`
}

// Country represents an individual country's data within the set.
type Country struct {
	Country   string `json:"country" example:"Ghana"` // Country involved
	Views     string `json:"views" example:"11609"`   // Number of pageviews
	Rank      int    `json:"rank"`
	ViewsCiel int    `json:"views_ciel"`
}

// TopByCountryResponse represents a container for the top-by-country resultset.
type TopByCountryResponse struct {
	Items []TopByCountry `json:"items"`
}
