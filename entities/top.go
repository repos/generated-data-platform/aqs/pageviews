package entities

// Top represents an entry in the API resultset.
type Top struct {
	Project  string    `json:"project" example:"en.wikipedia.org"` // Wikimedia project domain
	Access   string    `json:"access" example:"all-access"`        // Access method
	Year     string    `json:"year" example: "2023"`               // Year involved
	Month    string    `json:"month" example: "01"`                // Month involved
	Day      string    `json:"day" example: "01"`                  // Day involved
	Articles []Article `json:"articles"`
}

// Article represents article data in a Top result.
type Article struct {
	Article string `json:"article" example:"Jupiter"` // Page title with spaces replaced with underscores
	Views   int    `json:"views" example:"11609"`     // Number of pageviews
	Rank    int    `json:"rank"`
}

// TopResponse represents the API resultset.
type TopResponse struct {
	Items []Top `json:"items"`
}
