package entities

// PerProject represents one result from the per-project resultset.
type PerProject struct {
	Project     string `json:"project" example:"en.wikipedia.org"` // Wikimedia project domain
	Access      string `json:"access" example:"all-access"`        // Access method
	Agent       string `json:"agent" example:"user"`               // Agent type
	Granularity string `json:"granularity" example:"monthly"`      // Frequency of data
	Timestamp   string `json:"timestamp" example:"2021020100"`     // Timestamp in YYYYMMDDHH format
	Views       int    `json:"views" example:"11609"`              // Number of pageviews
}

// PerProjectResponse represents a container for the per-project resultset.
type PerProjectResponse struct {
	Items []PerProject `json:"items"`
}
