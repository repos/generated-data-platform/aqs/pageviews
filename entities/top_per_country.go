package entities

// TopPerCountry represents an entry in the API resultset.
type TopPerCountry struct {
	Country  string              `json:"country"`
	Access   string              `json:"access" example:"all-access"` // Access method
	Year     string              `json:"year" example: "2023"`        // Year involved
	Month    string              `json:"month" example: "01"`         // Month involved
	Day      string              `json:"day" example: "01"`           // Day involved
	Articles []PerCountryArticle `json:"articles"`
}

// PerCountryArticle represents the country data for a returned article.
type PerCountryArticle struct {
	Article   string `json:"article" example:"Jupiter"`          // Page title with spaces replaced with underscores
	Project   string `json:"project" example:"en.wikipedia.org"` // Wikimedia project domain
	ViewsCeil int    `json:"views_ceil"`
	Rank      int    `json:"rank"`
}

// TopPerCountryResponse represents the API resultset.
type TopPerCountryResponse struct {
	Items []TopPerCountry `json:"items"`
}
