package main

import (
	"context"
	"encoding/json"
	"net/http"
	"pageviews/logic"
	"strings"
	"time"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/valyala/fasthttp"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

// TopPerCountryHandler is an HTTP handler for top-per-country API requests.
type TopPerCountryHandler struct {
	logger  *logger.Logger
	session *gocql.Session
	logic   *logic.TopPerCountryLogic
	config  *Config
}

// API documentation
// @summary      Get pageviews for a top project by country.
// @router       /top-per-country/{country}/{access}/{year}/{month}/{day} [get]
// @description  Given a wiki page and a date range, returns a time series of pageview counts.
// @param        project      path  string  true  "Domain of a Wikimedia project"                                                example(en.wikipedia.org)
// @param        access       path  string  true  "Method of access" Enums(all-access, desktop, mobile-app, mobile-web)          example(all-access)
// @param        year         path  string  true  "The year to include, in YYYY format" example(2023)
// @param        month        path  string  true  "The month to include, in MM format"           example(01)
// @param        day          path  string  true  "The day to include, in DD format"            example(01)
// @produce      json
// @success      200 {object} entities.TopPerCountryResponse
func (s *TopPerCountryHandler) HandleFastHTTP(ctx *fasthttp.RequestCtx) {
	var err error

	country := ctx.UserValue("country").(string)
	access := strings.ToLower(ctx.UserValue("access").(string))
	year := ctx.UserValue("year").(string)
	month := ctx.UserValue("month").(string)
	day := ctx.UserValue("day").(string)

	c, cancel := context.WithTimeout(ctx, time.Duration(s.config.ContextTimeout)*time.Millisecond)

	response := s.logic.ProcessTopPerCountryLogic(c, ctx, country, access, year, month, day, ctx.Request.URI().String(), s.logger, s.session)
	defer cancel()

	if response == nil {
		return
	}
	ctx.SetStatusCode(http.StatusOK)

	var data []byte
	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Log(logger.ERROR, "Unable to marshal response object: %s", err)

		statusCode := http.StatusInternalServerError
		problemResp := aqsassist.CreateProblem(statusCode, err.Error(), string(ctx.Request.URI().RequestURI())).JSON()

		ctx.SetStatusCode(statusCode)
		ctx.SetBody(problemResp)
		aqsassist.InternalServerErrorWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: err.Error(),
			URL:    nil,
		})
		return
	}
	ctx.SetBody(data)
}
