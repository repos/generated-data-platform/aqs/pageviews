package main

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
)

func InvalidRouteTest(t *testing.T) {
	t.Run("should return 404 for invalid route", func(t *testing.T) {

		res, err := http.Get(apiTestURL("per-article/en.wikipedia/invalid-route"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusNotFound, res.StatusCode, "Wrong status code")
	})
}
